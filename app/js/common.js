jQuery(document).ready(function ($) {

  // Sticky Header onSroll
  $(function() {
    if(window.matchMedia('(max-width: 768px)').matches) {
      $('.site__header').addClass('skin--light');
    }else{
      $(window).scroll(function () {
        if ($(window).scrollTop() >= 100) {
          $('.site__header').addClass('skin--light');
        } else {
          $('.site__header').removeClass('skin--light');
        }
      });
    }
  });

  // Page scroll 2 id Init
  $("a[rel='m_PageScroll2id']").mPageScroll2id({
    highlightClass: 'highlight--underline',
    offset: 100,
  });

  // Nav open
  $('#nav-toggle').click(function () {
    $(this).toggleClass('is-active');
    $('.site__header').toggleClass('menu--toggle');
  });
  $('.site__header a').click(function () {
    $('.site__header').toggleClass('menu--toggle');
    $('#nav-toggle').toggleClass('is-active');
  });

  // Slick Init
  $('.presentation__slider').slick({
    arrows: false,
    dots: false,
    fade: true,
    autoplay: true,
    infinite: true,
  });

  // Animate.js
  $.fn.animated = function (inEffect) {
    $(this).each(function () {
      var ths = $(this);
      ths.css("opacity", "0").addClass("animated").waypoint(function (dir) {
        if (dir === "down") {
          ths.addClass(inEffect).css("opacity", "1");
        };
      }, {
        offset: "90%"
      });
    });
  };

  $(function () {
    $(".site__content h1").animated("fadeInDown");
    $(".site__content h2").animated("fadeInDown");

    $(".section-container").animated("fadeInDown");
    $(".main__content").animated("fadeInDown");
  });

});
